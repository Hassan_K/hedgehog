"use client" // this is a client component
import React, { CSSProperties, useState } from "react"
import Image from "next/image"
import { Link } from "react-scroll/modules"
import { HiArrowDown } from "react-icons/hi"
import { Icon } from '@iconify/react';
import AboutSection from "./AboutSection"
import { Text } from "./Text"

const HeroSection = () => {
  const [heroImage, setHeroImage] = useState('/hedgehog-removebg-preview.png');
  const [heroImageStyles, setHeroImageStyles] = useState({
    width: '400px',
    height: '400px',
    marginLeft: '-30px',
  });
 
const changeImage = (imgPath: string, styles: CSSProperties) => {
  console.log("Change image function called with path and styles:", imgPath, styles);

  // Ensure 'width', 'height', and 'marginLeft' are strings
  const newStyles = {
    width: styles.width ? styles.width.toString() : '', // Check if width is defined
    height: styles.height ? styles.height.toString() : '', // Check if height is defined
    marginLeft: styles.marginLeft ? styles.marginLeft.toString() : '', // Check if marginLeft is defined
  };

  // Set the hero image and styles
  setHeroImage(imgPath);
  setHeroImageStyles(newStyles);
};

const handleReset = () => {
  // Reset the hero image and styles
  setHeroImage('/hedgehog-removebg-preview.png');
  setHeroImageStyles({
    width: '400px',
    height: '400px',
    marginLeft: '-30px',
  });
};

 const handleDownload = () => {
    // Create a virtual anchor element
    const link = document.createElement('a');
    link.href = heroImage; // Set the href attribute to the currently rendered image
    link.download = 'image.png'; // Set the download attribute with a default filename
    document.body.appendChild(link); // Append the anchor element to the document body
    link.click(); // Simulate a click event to trigger the download
    document.body.removeChild(link); // Remove the anchor element from the document body
  };

  return (
    <div className=' max-w-[1240px] mx-auto grid md:grid-cols-2'>
  <div className=' ml-[40px] w-[300px] md:h-[300px] md:w-[290px] md:mr-[130px] relative flex justify-end  '>
   <div className=" hidden md:block px-10 w-32 border-r-2 border-[#764824] md:mt-2 md:w-1/2 md:mr-[-400px] md:h-[500px]" > </div>{/* Added inline style to move the image up and towards left */}

    <img className='w-[290px] mx-auto my-4  border-solid border-8 border-[#764824] rounded-xl ml-[10px] md:mr-[100px] md:h-[320px] md:mt-[30px]' src={heroImage} alt='/' />
   <button onClick={handleReset} className=' flex items-center justify-center text-center absolute bottom-[-60px] md:bottom-[-140px] border-4 border-[#764824] bg-[#fff] text-[#764824] w-[200px] md:w-[250px] rounded-full font-bold py-2 mb-4 mr-[50px] md:mr-[120px] '  style={{ fontFamily: "'Bitter', sans-serif", fontSize: "18px"}}>
  <Icon icon="system-uicons:reset-forward" width="20" style={{ color: "#764824", marginRight: "1px"}} />
  Reset
</button>

<button onClick={handleDownload} className=' flex items-center justify-center text-center absolute bottom-[-120px] md:bottom-[-200px] border-4 border-[#fff] bg-[#764824] text-[#fff] w-[200px] md:w-[250px] rounded-full font-bold py-2 mb-4 mr-[50px] md:mr-[120px]'  style={{ fontFamily: "'Bitter', sans-serif", fontSize: "18px"}}>    <Icon icon="material-symbols:download" width="20" style={{ color: "#fff", marginRight: "1px"}} />
      Download</button>
  </div>
  <div className="  ml-[68px] md:ml-[-120px]  md:mr-2 md:relative md:left-2 relative left-[-70px] mt-[120px] md:mt-[30px] w-[379px] md:w-[600px]   text-[#764824]" style={ {fontFamily: "'Bitter', sans-serif"}}>
          
           <h1 className="text-sm md:ml-[-4px] font-bold ml-20 md:mt-0" style={{fontSize:'20px'}}>CREATE YOUR SANIC</h1>
           
       <div >
       <AboutSection changeImage={changeImage} /> 
       </div>
         </div>
</div>
//     <section id="home" >
//       <div className="relative h-9 mt-72 bottom-20 ml-56 flex flex-col text-center items-center justify-center animate-fadeIn animation-delay-2 my-10 py-16 sm:py-32 md:py-48 md:flex-row md:space-x-4 md:text-left " style={{width:"90%"}}>
//       <div className=" px-10 w-32 border-r-2 border-[#764824] md:mt-2 md:w-1/2" style={{ marginTop: "-60px" }}> {/* Added inline style to move the image up and towards left */}
//         <div className=" border-solid border-8 border-[#764824] rounded-xl md:max-w-sm max-w-[300px] sm:mt-[-96px]" style={{ ...heroImageStyles , height: '400px',  marginLeft: "-30px"  }}>
//         <Image
//         src={heroImage}
//         alt="sanic"
//         width={400} // Set the width of the image for proper rendering
//         height={400} // Set the height of the image for proper rendering
//       />
//           </div>
//            <div className="mt-4 flex flex-col items-center ml-6">
//            <button onClick={handleReset} className="mb-2 py-2 text-center font-semibold w-96 border-4 text-[#764824] bg-[#fff] border-[#764824] rounded-full flex items-center justify-center" style={{ fontFamily: "'Bitter', sans-serif", fontSize: "22px", marginLeft: "-80px"  }}>
//   <Icon icon="system-uicons:reset-forward" width="30" style={{ color: "#764824", marginRight: "10px" , fontSize:'20px' ,fontWeight:"bolder" }} />
//   Reset
// </button>

// <button onClick={handleDownload} className="mb-2 py-2 text-center font-semibold w-96 border-4 text-[#fff] bg-[#764824] border-[#fff] rounded-full flex items-center justify-center" style={{ fontFamily: "'Bitter', sans-serif", fontSize: "22px", marginLeft: "-80px"  }}>
//   <Icon icon="material-symbols:download" width="30" style={{ color: "#dbdbdb", marginRight: "10px" , fontSize:'20px' ,fontWeight:"bolder" }} />
//   Download
// </button>
         

   
//   </div>
 
//         </div>
//         <div className="  md:mt-2 md:w-3/5 text-[#764824]" style={{marginRight:"320px", marginTop:"-200px", fontFamily: "'Bitter', sans-serif", width:"80%"}}>
          
//           <h1 className="text-sm font-bold ml-12 md:mt-0" style={{fontSize:'20px'}}>CREATE YOUR SANIC</h1>
           
//       <div >
//       <AboutSection changeImage={changeImage} /> 
//       </div>
//         </div>
//       </div>
      
//     </section>
  )
}

export default HeroSection
